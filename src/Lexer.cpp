#include "Lexer.hpp"

#include "Logger.hpp"

#include <tuple>

namespace blc {

void Lexer::tokenize(std::string const& compileUnit) {
    symbols = compileUnit.c_str();
    advance = 0;

    line = 0;
    col = 0;
    currentLine = symbols;

    while (parseToken()) {
        symbols += advance;
        col += advance;
        advance = 0;
        skipSpaces();
        if (isEof()) {
            return;
        }
    }

    char const* temp = currentLine;
    uint32_t charsUntilNewLine = 0;
    for (; *temp != '\n'; ++temp, ++charsUntilNewLine);

    std::string_view out(currentLine, charsUntilNewLine);
    BLG("{}", out);
    BLG("{:>{}}", "^", col + 1);
    BLG("Unknown token (line: {}, col: {})", line, col);
}

void Lexer::skipSpaces() {
    for (; std::isspace(*symbols); ++symbols) {
        ++col;
        if (*symbols == '\n') {
            ++line;
            col = 0;
            currentLine = symbols + 1;
        }
    }
}

bool Lexer::parseToken() {
    if (tokens.size() > 1) {
        auto const* last1 = std::get_if<Keyword>(&tokens[tokens.size() - 1]);
        auto const* last2 = std::get_if<Keyword>(&tokens[tokens.size() - 2]);
        if (last1 && last2 && *last1 == Keyword::operatorKeyword && *last2 == Keyword::defKeyword) {
            if (parseOperatorDefinition()) {
                operators.push_back(Operator{symbols, advance});
                tokens.emplace_back(Operator{symbols, advance});
                return true;
            }
            return false;
        }
    }

    if (Keyword keyword; parseKeyword(keyword)) {
        tokens.emplace_back(keyword);
        return true;
    }

    if (parseIdentidier()) {
        tokens.emplace_back(Identifier{symbols, advance});
        return true;
    }

    if (Punctuator punctuator; parsePunctuator(punctuator)) {
        tokens.emplace_back(punctuator);
        return true;
    }

    if (Operator operatorToken; parseOperator(operatorToken)) {
        tokens.emplace_back(operatorToken);
        return true;
    }

    return false;
}

bool Lexer::parseKeyword(Keyword& result) {
    for (auto const& [name, keyword] : keywords) {
        if (name == std::string_view{symbols, name.size()}) {
            advance = name.size();
            result = keyword;
            return true;
        }
    }

    return false;
}

bool Lexer::parsePunctuator(Punctuator& result) {
    for (auto const& [name, punctuator] : punctuators) {
        if (name == std::string_view{symbols, name.size()}) {
            advance = name.size();
            result = punctuator;
            return true;
        }
    }

    return false;
}

bool Lexer::parseOperatorDefinition() {
    if (not parseOperatorSymbol()) {
        return false;
    }

    while (parseOperatorSymbol());
    return true;
}

bool Lexer::parseOperatorSymbol() {
    std::array<bool, 256> validSym{};
    validSym['='] = true;

    char symbol = symbols[advance];

    if (validSym[symbol]) {
        ++advance;
        return true;
    }

    return false;
}

bool Lexer::parseOperator(Operator& result) {
    for (auto const& operatorItem : operators) {
        if (operatorItem.name == std::string_view{symbols, operatorItem.name.size()}) {
            advance = operatorItem.name.size();
            result = operatorItem;
            return true;
        }
    }

    return false;
}

bool Lexer::parseIdentidier() {
    if (!parseIdentifierNonDigit()) {
        return false;
    }

    while (parseRestIdentidier());

    return true;
}

bool Lexer::parseRestIdentidier() {
    if (parseIdentifierNonDigit()) {
        return true;
    } else if (parseDigit()) {
        return true;
    }

    return false;
}

bool Lexer::parseIdentifierNonDigit() {
    if (parseNonDigit()) {
        return true;
    }

    return false;
}

bool Lexer::parseNonDigit() {
    char symbol = symbols[advance];
    
    if (std::isalpha(symbol) or symbol == '_') {
        ++advance;
        return true;
    }

    return false;
}

bool Lexer::parseDigit() {
    char symbol = symbols[advance];

    if (std::isdigit(symbol)) {
        ++advance;
        return true;
    }

    return false;
}

} // namespace blc
