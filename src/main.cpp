#include "Lexer.hpp"
#include "Logger.hpp"

#include <fstream>
#include <sstream>

int main() {
    std::ifstream fin("S:/Projects/blc/assets/main.bv");
    std::stringstream buffer;
    buffer << fin.rdbuf();
    std::string program = buffer.str();

    blc::Lexer lexer;
    lexer.tokenize(program);
    BLG("{}", lexer.tokens);
}
