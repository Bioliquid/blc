#pragma once

#include "Token.hpp"

#include <vector>

namespace blc {

class Lexer {
public:
    void tokenize(std::string const&);

// private:
    void skipSpaces();

    bool parseKeyword(Keyword&);

    bool parsePunctuator(Punctuator&);

    bool parseOperatorDefinition();
    bool parseOperatorSymbol();
    bool parseOperator(Operator&);

    bool parseToken();
    bool parseIdentidier();
    bool parseRestIdentidier();
    bool parseIdentifierNonDigit();
    bool parseNonDigit();
    bool parseDigit();

    inline bool isEof() const { return (*symbols) == 0; }

    char const* symbols;
    int32_t     advance;
    std::vector<Token> tokens;

    uint32_t line;
    char const* currentLine;
    uint32_t col;
};

} // namespace blc
