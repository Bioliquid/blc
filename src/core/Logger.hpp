#pragma once

#include <fmt/ostream.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

template<size_t len>
constexpr char const* baseFilenameImpl(char const (&sz)[len], size_t n) {
    return (0 == n) ? sz : (sz[n] == '\\' || sz[n] == '/') ? sz + n + 1 : baseFilenameImpl(sz, --n);
}

template<size_t len>
constexpr char const* baseFilename(char const (&sz)[len]) {
    return baseFilenameImpl(sz, len - 1);
}

#define LOGFNAME baseFilename(__FILE__)

#define BLG(tempstr, ...) \
    fmt::print("{}[{}]: " tempstr "\n", LOGFNAME, __LINE__, __VA_ARGS__);
