#pragma once

#include <fmt/core.h>

#include <string>
#include <string_view>
#include <variant>
#include <array>
#include <vector>

namespace blc {

struct Identifier {
    Identifier(char const* ptr, int32_t count) : name(ptr, count) {}

    std::string_view name;
};

enum class Keyword {
      autoKeyword
    , boolKeyword
    , charKeyword
    , defKeyword
    , operatorKeyword
};

enum class Punctuator {
      semicolon
};

struct Operator {
    Operator() = default;
    Operator(char const* ptr, int32_t count) : name(ptr, count) {}

    std::string_view name;
};

using Token = std::variant<Identifier, Keyword, Punctuator, Operator>;

constexpr std::array<std::pair<std::string_view, Keyword>, 5> keywords = {
      std::make_pair("auto", Keyword::autoKeyword)
    , std::make_pair("bool", Keyword::boolKeyword)
    , std::make_pair("char", Keyword::charKeyword)
    , std::make_pair("def", Keyword::defKeyword)
    , std::make_pair("operator", Keyword::operatorKeyword)
};

constexpr std::array<std::pair<std::string_view, Punctuator>, 1> punctuators = {
      std::make_pair(";", Punctuator::semicolon)
};

inline std::vector<Operator> operators;

} // namespace blc

template <> struct fmt::formatter<blc::Identifier> : formatter<string_view> {
    template <typename FormatContext>
    auto format(blc::Identifier ident, FormatContext& ctx) const {
        return fmt::format_to(ctx.out(), "{}", ident.name);
    }
};

template <> struct fmt::formatter<blc::Keyword> : formatter<string_view> {
    template <typename FormatContext>
    auto format(blc::Keyword keyword, FormatContext& ctx) const {
        string_view name = "unknown";
        switch (keyword) {
        case blc::Keyword::autoKeyword: name = "auto"; break;
        case blc::Keyword::boolKeyword: name = "bool"; break;
        case blc::Keyword::charKeyword: name = "char"; break;
        case blc::Keyword::defKeyword: name = "def"; break;
        case blc::Keyword::operatorKeyword: name = "operator"; break;
        }
        return formatter<string_view>::format(name, ctx);
    }
};

template <> struct fmt::formatter<blc::Punctuator> : formatter<string_view> {
    template <typename FormatContext>
    auto format(blc::Punctuator punctuator, FormatContext& ctx) const {
        string_view name = "unknown";
        switch (punctuator) {
        case blc::Punctuator::semicolon: name = ";"; break;
        }
        return formatter<string_view>::format(name, ctx);
    }
};

template <> struct fmt::formatter<blc::Operator> : formatter<string_view> {
    template <typename FormatContext>
    auto format(blc::Operator ident, FormatContext& ctx) const {
        return fmt::format_to(ctx.out(), "{}", ident.name);
    }
};

template <> struct fmt::formatter<blc::Token> : formatter<string_view> {
    template <typename FormatContext>
    auto format(blc::Token token, FormatContext& ctx) const {
        switch (token.index()) {
        case 0: return fmt::format_to(ctx.out(), "Identifier<{}>", std::get<0>(token));
        case 1: return fmt::format_to(ctx.out(), "Keyword<{}>", std::get<1>(token));
        case 2: return fmt::format_to(ctx.out(), "Punctuator<{}>", std::get<2>(token));
        case 3: return fmt::format_to(ctx.out(), "Operator<{}>", std::get<3>(token));
        }
        return fmt::format_to(ctx.out(), "unknown");
    }
};
